//********************************************************************
// Name: app.js
// Desc: Main file of notes-app
// Auth: Franco Rosatti
// Date: Jul-2020
// Vers: 1.0
//********************************************************************

//Include core package
const fs = require('fs');

//Include third party package
const validator = require('validator'); //we must first install it with 'npm i package_name'
const yargs = require('yargs');

//Include own package
const service = require('./service.js');

//Config
yargs.version('1.0')
yargs.command({
    command: ['add', 'a', 'n', 'new'],
    describe: 'Add a new note',
    builder: {
        title: {
            describe: 'Note title',
            demandOption: true,
            type: 'string'
        },
        body: {
            describe: 'Note body',
            demandOption: true,
            type: 'string'
        }
    },
    handler(argv) {
        service.addNote(argv.title, argv.body)
    }
});
yargs.command({
    command: ['rm', 'del', 'remove', 'delete'],
    describe: 'Remove a note',
    title: {
        describe: 'Note title',
        demandOption: true,
        type: 'string'
    },
    handler(argv) {
        service.removeNote(argv.title);
    }
});
yargs.command({
    command: ['list', 'l'],
    describe: 'List all notes',
    handler() {
        service.listNotes();
    }
});
yargs.command({
    command: ['rd', 'r', 'read'],
    describe: 'Read a note',
    title: {
        describe: 'Note title',
        demandOption: true,
        type: 'string'
    },
    handler(argv) {
        service.readNote(argv.title);
    }
});

yargs.parse()