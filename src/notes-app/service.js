//********************************************************************
// Name: service.js
// Desc: Services for notes app
// Auth: Franco Rosatti
// Date: Jul-2020
// Vers: 1.0
//********************************************************************

//Include core package
const fs = require('fs')
const chalk = require('chalk');
const { debug } = require('console');

//PRIVATE
const filename = 'notes.txt';

const loadNotes = () => {
    try {
        if (fs.existsSync(filename)) {
            return JSON.parse(fs.readFileSync(filename));
        } else {
            return [];
        }
    } catch (e) {
        return [];
    }
}

const saveNotes = (notes) => {
    fs.writeFileSync(filename, JSON.stringify(notes))
}

//PUBLIC
const listNotes = () => {
    console.log(chalk.blue.underline.bold('My Notes'));
    loadNotes().forEach(note => console.log(chalk.magenta("•", note.title)));
}

const addNote = (title, body) => {
    const notes = loadNotes();

    //Check Duplicate Title
    //const duplicates = notes.filter((note) => note.title === title); //ineficient because it will always go through the whole array
    const duplicate = notes.find(note => note.title === title);

    if (duplicate === undefined) {
        notes.push({ title: title, body: body });
        saveNotes(notes);
        console.log(chalk.green('New note added'));
    } else {
        console.log(chalk.red('Note title already taken'));
    }
}

const removeNote = (title) => {
    const notes = loadNotes();

    //Find note
    const filtered = notes.filter((note) => note.title !== title);

    if (filtered.length === notes.length) {
        console.log(chalk.red('Note not found'));
    } else {
        saveNotes(filtered);
        console.log(chalk.green('Note removed:', title));
    }
}

const readNote = (title) => {
    const n = loadNotes().find(note => note.title === title);

    //debugger;
    //node inspect app.js read --title="Bakery"
    //node --inspect-brk app.js read --title="Bakery"

    if(n === undefined){
        console.log(chalk.red('Note not found'));
    } else {
        console.log(chalk.blue("Title:", n.title));
        console.log(chalk.magenta("Body:", n.body));
    }
}

//EXPORT
module.exports = { listNotes, addNote, removeNote, readNote };