const path = require('path')
const express = require('express');
const hbs = require('hbs');

const geocode = require('./utils/geocode');
const forecast = require('./utils/forecast');

const app = express();

// Define paths for Express config
const publicDir = path.join(__dirname, '../public');
const viewsDir = path.join(__dirname, '../templates/views');
const partialsDir = path.join(__dirname, '../templates/partials');

//Setup handlebars engine and views location
app.set('view engine', 'hbs');
app.set('views', viewsDir);
hbs.registerPartials(partialsDir);

//Setup static directory to serve
app.use(express.static(publicDir));

app.get('/', (req, res) => {
    res.render('index', {
        title: 'Weather',
        author: 'Franco Rosatti'
    });
});

app.get('/about', (req, res) => {
    res.render('about', {
        title: 'About me',
        author: 'Franco Rosatti'
    });
});

app.get('/help', (req, res) => {
    res.render('help', {
        title: 'Help',
        msg: 'This is a help message',
        author: 'Franco Rosatti'
    });
});

app.get('/weather', (req, res) => {
    if (!req.query.address) {
        return res.send({
            error: 'You must provide an address'
        });
    }

    geocode.geocode(req.query.address, (error, { location, latitude, longitude } = {}) => {
        if (error) return res.send({ error });

        forecast.forecast(latitude, longitude, (error, forecast) => {
            if (error) return res.send({ error });

            return res.send({
                address: req.query.address,
                location,
                latitude,
                longitude,
                forecast
            });
        });
    });
});

app.get('/help/*', (req, res) => {
    res.render('404', {
        title: '404 - Not Found',
        msg: 'Help article not found',
        author: 'Franco Rosatti'
    });
});

app.get('*', (req, res) => {
    res.render('404', {
        title: '404 - Not Found',
        msg: 'Page not found',
        author: 'Franco Rosatti'
    });
});

app.listen(3000, () => {
    console.log('Server is up in port 3000');
});