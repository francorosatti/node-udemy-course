const weatherForm = document.querySelector('form');
const searchInput = document.querySelector('input');
const msg1 = document.querySelector('#msg1');

weatherForm.addEventListener('submit', (event) => {
    event.preventDefault();

    const location = searchInput.value;

    msg1.style.color = '#333';
    msg1.textContent = 'Loading...';

    fetch(`http://localhost:3000/weather?address=${location}`).then((response) => {
    response.json().then((data) => {
        if (data.error) {
            msg1.style.color = '#f00';
            msg1.textContent = data.error;
        } else {
            msg1.style.color = '#333';
            msg1.textContent = `${data.location}. ${data.forecast.weather_description}. Temperature: ${data.forecast.temperature}°C. Feels Like: ${data.forecast.feels_like}°C.`;
        }
    });
});
});