//Traditional func
const square = function (x) {
    return x * x;
}

//Arrow func
const arrowSquare = (x) => {
    return x * x;
}

//Simplified arrow func
const arrowSquare2 = (x) => x * x;

const event = {
    name: 'Birthday Party',
    guestList: ['Alex', 'Mike', 'Amy'],
    printGuestList: function () {
        console.log('Guest list for ' + this.name);
    },
    printGuestList2: () => { //this is not gonna work
        console.log('Guest list for ' + this.name); //this is undefined in an arrow func
    },
    printGuestList3() { //this works
        console.log('Guest list for ' + this.name);
    },
    printGuestList4() {
        console.log(this.name);
        this.guestList.forEach(function (guest) {
            console.log(guest + ' is attending ' + this.name); //here this is undefined
        });
    },
    printGuestList5() {
        console.log(this.name);
        this.guestList.forEach((guest) => {
            console.log(guest + ' is attending ' + this.name); //here this is undefined
        });
    }
}

//ARROW FUNCTIONS DONT BIND THEIR OWN "THIS" VALUE

event.printGuestList()
event.printGuestList2()
event.printGuestList3()
event.printGuestList4()
event.printGuestList5()