const fs = require('fs');

const filename = '1-json.json';

/*
const book = {
    title: 'Animal Farm',
    author: 'George Orwell'
}

const bookJSON = JSON.stringify(book);
//console.log(bookJSON);
fs.writeFileSync(filename, bookJSON);


const parsed = JSON.parse(fs.readFileSync(filename));
console.log(parsed.author);
*/

const parsed = JSON.parse(fs.readFileSync(filename));
parsed.age = 55
parsed.planet = 'Mars'
parsed.name = 'Robert'
fs.writeFileSync(filename, JSON.stringify(parsed));