//Object property shorthand

const name = 'Andrew';
const userAge = 27;

const user = {
    name: name,
    age: userAge,
    location: 'Philadelphia'
}

const user2 = {
    name,
    age: userAge, //can't do it because is not called the same
    location: 'Philadelphia'
}

console.log(user);

//Object destructuring

const product = {
    label: 'Red notebook',
    price: 3,
    stock: 201,
    salePrice: undefined
}

//Traditional
const label = product.label;
//const stock = product.stock;

//ES6
const {label: productLabel, stock, rating = 5} = product
//productLabel: we are renaming a property
//rating:   didn't exist in product but I can fill it with a value,
//          it will only be used if rating doesn't exist
console.log(productLabel);
console.log(stock);
console.log(rating);

//We can deconstruct inside the function or...
//const transaction = (type, myProduct) => {
//    const {label, stock} = myProduct;
//}
//we can deconstruct in argument list
const transaction = (type, { label, stock }) => {
    console.log(type, label, stock);
}

transaction('order', product);