//HTTP REQUESTS WITHOUT USING POSTMAN-REQUEST PACKAGE

const http = require('http');

const url = `http://api.weatherstack.com/current?access_key=0332682c9cea326a9384adcf41dda9fb&query=-34.59,-58.38&units=m`;

const request = http.request(url, (response) => {
    let data = '';

    response.on('data', (chunk) => {
        data = data + chunk.toString()
    });

    response.on('end', () => {
        const body = JSON.parse(data);
        console.log(`Current Temperature: ${body.current.temperature}°C`);
    });

});

request.on('error', (error) => {
    console.log(error);
});

request.end();