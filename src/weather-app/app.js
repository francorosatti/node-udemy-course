const request = require('postman-request');
const chalk = require('chalk');
const geocode = require('./utils/geocode');
const forecast = require('./utils/forecast');

function printError(msg) {
    console.log(chalk.red.inverse(msg));
}

function searchForecast(location) {
    if (!location) {
        return printError('Enter a valid location');
    }
    geocode.geocode(location, (error, { location, latitude, longitude } = {}) => { //added an optional value for deconstruction
        if (error) {
            return printError(error);
        } else {
            console.log(`Place: ${location}`)
            console.log(`Latitude: ${latitude}`);
            console.log(`Longitude: ${longitude}`);

            forecast.forecast(latitude, longitude, (error, { weather_description, temperature, feels_like } = {}) => {
                if (error) {
                    return printError(error);
                } else {
                    console.log(`Description: ${weather_description}`)
                    console.log(`Temperature: ${temperature}`);
                    console.log(`Feels Like: ${feels_like}`);
                }
            });
        }
    });
}

searchForecast(process.argv[2]); //arg[2] is the city the user enter
