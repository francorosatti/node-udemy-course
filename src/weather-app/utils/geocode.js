const request = require('postman-request');

const geocode = (address, callback) => {
    const url = `https://api.mapbox.com/geocoding/v5/mapbox.places/${encodeURIComponent(address)}.json?access_token=pk.eyJ1IjoiZnJhbmNvcm9zYXR0aSIsImEiOiJja2N5c3E4M2gwNzY2Mndtb2VxZWl2NzZoIn0.Ju2bGIIBk7eh0skIaGh03g&limit=1`

    request({ url, json: true }, (error, { body }) => {
        if (error) {
            callback('Unable to connect to GeoLocation Service!');
        } else if (body.message) {
            callback(`Bad Request: ${body.message}`);
        } else if (body.features.length <= 0) {
            callback('Unable to convert location to coordinates!');
        } else {
            callback(undefined, {
                location: body.features[0].place_name,
                latitude: body.features[0].center[1],
                longitude: body.features[0].center[0]
            })
        }
    });
}

module.exports = { geocode };