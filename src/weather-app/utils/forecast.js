const request = require('postman-request');

const forecast = (latitude, longitude, callback) => {
    const url = `http://api.weatherstack.com/current?access_key=0332682c9cea326a9384adcf41dda9fb&query=${latitude},${longitude}&units=m`

    request({ url , json: true }, (error, { body }) => {
        if (error) {
            callback('Unable to connect to Weather Service!');
        } else if (body.error) {
            callback('Unable to find location!');
        } else {
            callback(undefined, {
                weather_description: body.current.weather_descriptions[0],
                temperature: body.current.temperature,
                feels_like: body.current.feelslike
            });
        }
    })
}

module.exports = { forecast };